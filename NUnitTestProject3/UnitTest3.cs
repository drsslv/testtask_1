using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
#region TaskDescription
//1. ������� ������� � ���������� �� ���� �����.
//2. ����� �� yandex.ru.
//3. ������� � ������ ������
//4. ������� ������ �����������
//5. ������� ������ ��������� ��������
//6. ������� ���������� �� ����
//7. ���������, ��� �������� �� �������� ������������� �����
#endregion

namespace NUnitTestProject3
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }
        #region Steps
        //[Test]
        //public void LaunchBrowserAndMaximizeScreen()
        //{
        //    IWebDriver driver = new ChromeDriver();
        //    driver.Manage().Window.Maximize();

        //}

        //[Test]
        //public void GoToUrl_Yandex_ru()
        //{
        //    IWebDriver driver = new ChromeDriver();
        //    driver.Manage().Window.Maximize();

        //    driver.Navigate().GoToUrl("https://yandex.ru");
        //}

        //[Test]
        //public void GoTo_YandexMarket()
        //{
        //    IWebDriver driver = new ChromeDriver();
        //    driver.Manage().Window.Maximize();

        //    driver.Navigate().GoToUrl("https://yandex.ru");

        //    driver.FindElement(By.LinkText("������")).Click();
        //}

        //[Test]
        //public void GoTo_ElectronicsSection()
        //{
        //    IWebDriver driver = new ChromeDriver();
        //    driver.Manage().Window.Maximize();

        //    driver.Navigate().GoToUrl("https://yandex.ru");

        //    string oldTab = driver.CurrentWindowHandle;
        //    driver.FindElement(By.LinkText("������")).Click();

        //    List<string> newTab = new List<string>(driver.WindowHandles);
        //    newTab.Remove(oldTab);
        //    driver.SwitchTo().Window(newTab[0]);

        //    driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div/div/div/noindex/div/div/div/div[2]/div/div[1]/div[3]/div/a")).Click();
        //}

        //[Test]
        //public void GoTo_MobilePhonesSection()
        //{
        //    IWebDriver driver = new ChromeDriver();
        //    driver.Manage().Window.Maximize();

        //    driver.Navigate().GoToUrl("https://yandex.ru");

        //    string oldTab = driver.CurrentWindowHandle;
        //    driver.FindElement(By.LinkText("������")).Click();

        //    List<string> newTab = new List<string>(driver.WindowHandles);
        //    newTab.Remove(oldTab);
        //    driver.SwitchTo().Window(newTab[0]);

        //    driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div/div/div/noindex/div/div/div/div[2]/div/div[1]/div[3]/div/a")).Click();

        //    driver.FindElement(By.LinkText("��������� ��������")).Click();
        //}

        //[Test]
        //public void PickSortByPrice()
        //{
        //    IWebDriver driver = new ChromeDriver();
        //    driver.Manage().Window.Maximize();

        //    driver.Navigate().GoToUrl("https://yandex.ru");

        //    string oldTab = driver.CurrentWindowHandle;
        //    driver.FindElement(By.LinkText("������")).Click();

        //    List<string> newTab = new List<string>(driver.WindowHandles);
        //    newTab.Remove(oldTab);
        //    driver.SwitchTo().Window(newTab[0]);

        //    driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div/div/div/noindex/div/div/div/div[2]/div/div[1]/div[3]/div/a")).Click();

        //    driver.FindElement(By.LinkText("��������� ��������")).Click();

        //    driver.FindElement(By.LinkText("�� ����")).Click();
        //}
        #endregion

        [Test]
        public void CheckThatSortIsRight()
        {
            using (IWebDriver driver = new ChromeDriver())
            {
                driver.Manage().Window.Maximize();

                driver.Navigate().GoToUrl("https://yandex.ru");

                string oldTab = driver.CurrentWindowHandle;
                driver.FindElement(By.LinkText("������")).Click();

                List<string> newTab = new List<string>(driver.WindowHandles);
                newTab.Remove(oldTab);
                driver.SwitchTo().Window(newTab[0]);

                driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div/div/div/noindex/div/div/div/div[2]/div/div[1]/div[3]/div/a")).Click();

                driver.FindElement(By.LinkText("��������� ��������")).Click();

                driver.FindElement(By.LinkText("�� ����")).Click();
                System.Threading.Thread.Sleep(10000);
                List<IWebElement> phones = driver.FindElements(By.CssSelector("a .price")).ToList();

                foreach (var i in phones)
                {
                    Console.WriteLine(i.Text);
                }
                var isRight = true;
                for (int i = 0; i < phones.Count - 1; i++)
                {
                    if ((Convert.ToInt32(phones[i].Text)) < Convert.ToInt32(phones[i++].Text)) continue;
                    else
                    {
                        isRight = false;
                        break;
                    }
                }
                Assert.AreEqual(true, isRight);
            }

        }
    }
}