using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
#region TaskDescription
//������� 1.
//� ������� Selenium + C# ���������������� ��� �������� ��������.

//1. ������� ������� � ���������� �� ���� �����.
//2. ����� �� yandex.ru.
//3. ������� � ������ ������
//4. ������� ������ �����������
//5. ������� ������ ��������� ��������
//6. ������� � ����������� �����
//7. ������ �������� ������ �� 20000 ������. (������� ������� ����� �� �������� ���� �� 25000 �� 30000 �.� ������ ��������� ���������� ����� ������� )
//8. ������� �������������� Apple � Samsung 
//9. ������ ������ ���������. (����� ������ �� ����� ���)
//10. ���������, ��� ��������� �� �������� 12. ( ��������� �� ����� ���� 28 ) 
//11. ��������� ������ ������� � ������.
//12. � ��������� ������ ������ ����������� ��������.
//13. ����� � ���������, ��� ������������ ������ ������������� ������������ ��������.
#endregion
namespace NUnitTestProject1
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }
        #region Steps
        //[Test]
        //public void LaunchBrowserAndMaximizeScreen()
        //{
        //    IWebDriver driver = new ChromeDriver();
        //    driver.Manage().Window.Maximize();

        //}

        //[Test]
        //public void GoToUrl_Yandex_ru()
        //{
        //    IWebDriver driver = new ChromeDriver();
        //    driver.Manage().Window.Maximize();

        //    driver.Navigate().GoToUrl("https://yandex.ru");
        //}

        //[Test]
        //public void GoTo_YandexMarket()
        //{
        //    IWebDriver driver = new ChromeDriver();
        //    driver.Manage().Window.Maximize();

        //    driver.Navigate().GoToUrl("https://yandex.ru");

        //    driver.FindElement(By.LinkText("������")).Click();
        //}


        //[Test]
        //public void GoTo_ElectronicsSection()
        //{
        //    IWebDriver driver = new ChromeDriver();
        //    driver.Manage().Window.Maximize();

        //    driver.Navigate().GoToUrl("https://yandex.ru");

        //    string oldTab = driver.CurrentWindowHandle;
        //    driver.FindElement(By.LinkText("������")).Click();

        //    List<string> newTab = new List<string>(driver.WindowHandles);
        //    newTab.Remove(oldTab);
        //    driver.SwitchTo().Window(newTab[0]);

        //    driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div/div/div/noindex/div/div/div/div[2]/div/div[1]/div[3]/div/a")).Click();
        //}

        //[Test]
        //public void GoTo_MobilePhonesSection()
        //{
        //    IWebDriver driver = new ChromeDriver();
        //    driver.Manage().Window.Maximize();

        //    driver.Navigate().GoToUrl("https://yandex.ru");

        //    string oldTab = driver.CurrentWindowHandle;
        //    driver.FindElement(By.LinkText("������")).Click();

        //    List<string> newTab = new List<string>(driver.WindowHandles);
        //    newTab.Remove(oldTab);
        //    driver.SwitchTo().Window(newTab[0]);

        //    driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div/div/div/noindex/div/div/div/div[2]/div/div[1]/div[3]/div/a")).Click();

        //    driver.FindElement(By.LinkText("��������� ��������")).Click();
        //}

        //[Test]
        //public void SetPriceFrom_25000_TO_30000()
        //{
        //    IWebDriver driver = new ChromeDriver();
        //    driver.Manage().Window.Maximize();

        //    driver.Navigate().GoToUrl("https://yandex.ru");

        //    string oldTab = driver.CurrentWindowHandle;
        //    driver.FindElement(By.LinkText("������")).Click();

        //    List<string> newTab = new List<string>(driver.WindowHandles);
        //    newTab.Remove(oldTab);
        //    driver.SwitchTo().Window(newTab[0]);

        //    driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div/div/div/noindex/div/div/div/div[2]/div/div[1]/div[3]/div/a")).Click();

        //    driver.FindElement(By.LinkText("��������� ��������")).Click();

        //    driver.FindElement(By.XPath("//*[@id='glpricefrom']")).SendKeys("25000");
        //    driver.FindElement(By.XPath("//*[@id='glpriceto']")).SendKeys("30000");

        //}

        //[Test]
        //public void CheckAppleAndSamsungCheckboxes()
        //{
        //    IWebDriver driver = new ChromeDriver();
        //    driver.Manage().Window.Maximize();

        //    driver.Navigate().GoToUrl("https://yandex.ru");

        //    string oldTab = driver.CurrentWindowHandle;
        //    driver.FindElement(By.LinkText("������")).Click();

        //    List<string> newTab = new List<string>(driver.WindowHandles);
        //    newTab.Remove(oldTab);
        //    driver.SwitchTo().Window(newTab[0]);

        //    driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div/div/div/noindex/div/div/div/div[2]/div/div[1]/div[3]/div/a")).Click();

        //    driver.FindElement(By.LinkText("��������� ��������")).Click();

        //    driver.FindElement(By.XPath("//*[@id='glpricefrom']")).SendKeys("25000");
        //    driver.FindElement(By.XPath("//*[@id='glpriceto']")).SendKeys("30000");



        //    driver.FindElement(By.XPath("//*[@class='NVoaOvqe58' and contains(text(),'Apple')]")).Click();
        //    driver.FindElement(By.XPath("//*[@class='NVoaOvqe58' and contains(text(),'Samsung')]")).Click();

        //}

        //[Test]
        //public void GetCountPhones()
        //{
        //    IWebDriver driver = new ChromeDriver();

        //    driver.Manage().Window.Maximize();

        //    driver.Navigate().GoToUrl("https://yandex.ru");

        //    string oldTab = driver.CurrentWindowHandle;
        //    driver.FindElement(By.LinkText("������")).Click();

        //    List<string> newTab = new List<string>(driver.WindowHandles);
        //    newTab.Remove(oldTab);
        //    driver.SwitchTo().Window(newTab[0]);

        //    driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div/div/div/noindex/div/div/div/div[2]/div/div[1]/div[3]/div/a")).Click();

        //    driver.FindElement(By.LinkText("��������� ��������")).Click();

        //    driver.FindElement(By.XPath("//*[@id='glpricefrom']")).SendKeys("25000");
        //    driver.FindElement(By.XPath("//*[@id='glpriceto']")).SendKeys("30000");



        //    driver.FindElement(By.XPath("//*[@class='NVoaOvqe58' and contains(text(),'Apple')]")).Click();

        //    driver.FindElement(By.XPath("//*[@class='NVoaOvqe58' and contains(text(),'Samsung')]")).Click();
        //    System.Threading.Thread.Sleep(5000);
        //    List<IWebElement> phones = driver.FindElements(By.CssSelector(".n-snippet-list h3 a")).ToList();
        //    foreach (var i in phones)
        //    {
        //        Console.WriteLine(i.Text);
        //    }

        //    var countExpectedResult = 12;
        //    Assert.AreEqual(countExpectedResult, phones.Count);
        //}

        //[Test]
        //public void RememberFirstPhone_AND_EnterThisValueInTheInput()
        //{
        //    IWebDriver driver = new ChromeDriver();

        //    driver.Manage().Window.Maximize();

        //    driver.Navigate().GoToUrl("https://yandex.ru");

        //    string oldTab = driver.CurrentWindowHandle;
        //    driver.FindElement(By.LinkText("������")).Click();

        //    List<string> newTab = new List<string>(driver.WindowHandles);
        //    newTab.Remove(oldTab);
        //    driver.SwitchTo().Window(newTab[0]);

        //    driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div/div/div/noindex/div/div/div/div[2]/div/div[1]/div[3]/div/a")).Click();

        //    driver.FindElement(By.LinkText("��������� ��������")).Click();

        //    driver.FindElement(By.XPath("//*[@id='glpricefrom']")).SendKeys("25000");
        //    driver.FindElement(By.XPath("//*[@id='glpriceto']")).SendKeys("30000");



        //    driver.FindElement(By.XPath("//*[@class='NVoaOvqe58' and contains(text(),'Apple')]")).Click();

        //    driver.FindElement(By.XPath("//*[@class='NVoaOvqe58' and contains(text(),'Samsung')]")).Click();
        //    System.Threading.Thread.Sleep(5000);
        //    List<IWebElement> phones = driver.FindElements(By.CssSelector(".n-snippet-list h3 a")).ToList();
        //    foreach (var i in phones)
        //    {
        //        Console.WriteLine(i.Text);
        //    }

        //    var countExpectedResult = 11;
        //    Assert.AreEqual(countExpectedResult, phones.Count);

        //    var firstItem = phones[0].Text;

        //    driver.FindElement(By.Id("header-search")).SendKeys(firstItem + OpenQA.Selenium.Keys.Enter);

        //}
        #endregion
        [Test]
        public void CheckThatRecievedValueisEqualToExist()
        {
            using (IWebDriver driver = new ChromeDriver())
            {
                driver.Manage().Window.Maximize();

                driver.Navigate().GoToUrl("https://yandex.ru");

                string oldTab = driver.CurrentWindowHandle;
                driver.FindElement(By.LinkText("������")).Click();

                List<string> newTab = new List<string>(driver.WindowHandles);
                newTab.Remove(oldTab);
                driver.SwitchTo().Window(newTab[0]);

                driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div/div/div/noindex/div/div/div/div[2]/div/div[1]/div[3]/div/a")).Click();

                driver.FindElement(By.LinkText("��������� ��������")).Click();

                driver.FindElement(By.XPath("//*[@id='glpricefrom']")).SendKeys("25000");
                driver.FindElement(By.XPath("//*[@id='glpriceto']")).SendKeys("30000");



                driver.FindElement(By.XPath("//*[@class='NVoaOvqe58' and contains(text(),'Apple')]")).Click();

                driver.FindElement(By.XPath("//*[@class='NVoaOvqe58' and contains(text(),'Samsung')]")).Click();
                System.Threading.Thread.Sleep(5000);
                List<IWebElement> phones = driver.FindElements(By.CssSelector(".n-snippet-list h3 a")).ToList();
                foreach (var i in phones)
                {
                    Console.WriteLine(i.Text);
                }

                var countExpectedResult = 11; //(might be 12 or 11 )
                Assert.AreEqual(countExpectedResult, phones.Count);

                var firstItem = phones[0].Text;

                driver.FindElement(By.Id("header-search")).SendKeys(firstItem + OpenQA.Selenium.Keys.Enter);

                System.Threading.Thread.Sleep(5000);
                List<IWebElement> resultPhones = driver.FindElements(By.CssSelector(".n-snippet-list h3  a")).ToList();
                var resultPhone = resultPhones[0].Text;
                Assert.AreEqual(firstItem, resultPhone);
            }


        }

    }
}
